# Vue génerales
Ceci contient des fichiers de configuration pour prendre en main les objets utilisés par Kubernetes.
Il est constitué d'une série de tp suivit par une évaluation générales

## tp_pod
 Ici on voit comment déployer un pod basique
## tp_service
Ici on déploie deux pods pour exécuter le serveur Nginx.
Par la suite on créer deux services:
- Un service de type ClusterIP qui rend le serveur accessible uniquement à l'intérieur de cluster.
- Un service de type NodePort qui attache le port 80 du serveur web sur un port supérieur ou égale à 30000 sur tous les noeuds du cluster.
## tp_volumes
Cette partie montre comment on peut reserveur un volume en utilisant le PersistantVolumClaim et associer ce volume a un Pod
## tp_secret
Ici on crée des secrets dans kubernetes en utilisant soit un fichier ``` .env ``` soit un fichier normale. Les secrets sont comme des configMap chiffré
## tp_scheduler
Dans cette partie, on deploie des pod en spécifiant des contraintes. Le premier est déployer uniquemenst sur des noeuds qui possède un disque dur SSD et le second sur le meme noeud qu'un pod ayant le label ```app:web```
## tp_deployement
Dans ce TP, on deploie:
- Un DaemonSet qui s'assure qu'une et une seule instance du pod est en exécution sur tous les noeuds
- Un Deployement avec plussieurs replicas
## tp_configmap
Ici on crée:
- des variable d'environnement,
- Un fichier de configuration
## eval

L'évaluation consiste en:
Déployer deux pods dans lesquel, l'un crée regulièrement un fichier index.html dans un volume partager et le second pod rend ce fichier visible grace à un serveur nginx. Le serveur web est consultable en dehors du cluster.
